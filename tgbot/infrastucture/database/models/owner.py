from sqlalchemy import Column, Integer, VARCHAR, TIMESTAMP, func, BIGINT

from tgbot.infrastucture.database.models.base import Base



class Owner(Base):
    __tablename__ = 'owners'

    id_user = Column(BIGINT, nullable=False)
    id_contract = Column(BIGINT, nullable=False)
    id_owner = Column(Integer, primary_key=True, autoincrement=True)
    address = Column(VARCHAR(250), nullable=True)

