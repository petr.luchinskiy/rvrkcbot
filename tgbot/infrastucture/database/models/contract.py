from sqlalchemy import Column, Integer, VARCHAR, TIMESTAMP, func, BIGINT, Date

from tgbot.infrastucture.database.models.base import Base



class Contract(Base):
    __tablename__ = 'contracts'

    contract_id = Column(BIGINT, primary_key=True, nullable=False)
    street = Column(VARCHAR(150))
    house = Column(VARCHAR(150))
    flat = Column(VARCHAR(100))
    fam = Column(VARCHAR(100))
    name = Column(VARCHAR(100))
    patronomic = Column(VARCHAR(100))
    hvstek = Column(VARCHAR(100))
    polivtek = Column(VARCHAR(100))
    penyatek = Column(VARCHAR(100))
    actual = Column(Date)

