from sqlalchemy import Column, Integer, VARCHAR, TIMESTAMP, func, BIGINT

from tgbot.infrastucture.database.models.base import Base



class Reviews(Base):
    __tablename__ = 'reviews'

    id_review = Column(Integer, primary_key=True, autoincrement=True)
    id_user = Column(BIGINT, nullable=False)
    text_review = Column(VARCHAR(3000), nullable=True)
    full_name = Column(VARCHAR(250), nullable=True)
    created_at = Column(TIMESTAMP, server_default=func.now())
