from sqlalchemy import Column, BIGINT, VARCHAR, TIMESTAMP, ForeignKey, Date
from sqlalchemy.sql import func

from tgbot.infrastucture.database.models.base import Base


class Counter(Base):
    __tablename__ = 'counters'

    contract_id = Column(BIGINT )
    counter_id = Column(BIGINT, primary_key=True, nullable=False)
    type_counters = Column(VARCHAR(200))
    factory_numbers = Column(VARCHAR(200))
    next_verification_date = Column(Date)
    place = Column(VARCHAR(100))
    date_accept = Column(Date)
    actual = Column(Date)
