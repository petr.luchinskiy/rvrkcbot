from sqlalchemy import insert, select, delete
from sqlalchemy.orm import aliased, join

from tgbot.infrastucture.database.models.owner import Owner


async def create_owner(session, id_user, id_contract, address=None):
    stmt = insert(Owner).values(
        id_user=id_user,
        id_contract=id_contract,
        address=address
    )
    await session.execute(stmt)

async def delete_owner(session, id_user, id_contract):
    stmt = delete(
        Owner
    ).where(
        Owner.id_user == id_user,
        Owner.id_contract == id_contract
    )
    await session.execute(stmt)
    await session.commit()


async def select_owner(session):
    # Right JOIN is a LEFT join, with tables swapped
    stmt = select(
        Owner.user_id.label('user')  # We can use 'label' to give an alias to the column
    )
    result = session.execute(stmt)
    return result.all()


async def select_default_owner(session, contract_id):
    stmt = select(
        Owner.id_contract, Owner.id_user, Owner.address
    ).where(
        Owner.id_user == contract_id)
    result = await session.execute(stmt)
    return result.all()
