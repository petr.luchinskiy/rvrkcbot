from sqlalchemy import select
from tgbot.infrastucture.database.models.contract import Contract

async def select_contract(session, contract_id):
    stmt = select(
        Contract.contract_id, Contract.fam, Contract.street, Contract.house, Contract.flat
    ).where(
        Contract.contract_id == contract_id)
    result = await session.execute(stmt)
    return result.one_or_none()


