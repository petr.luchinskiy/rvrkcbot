from sqlalchemy import insert, select
from sqlalchemy.orm import aliased, join

from tgbot.infrastucture.database.models.reviews import Reviews

async def create_review(session, text_review, id_user, full_name):
    stmt = insert(Reviews).values(
        id_user=id_user,
        text_review=text_review,
        full_name=full_name
    )
    await session.execute(stmt)

