from aiogram import types
from aiogram import Dispatcher
from aiogram.types import Message
from aiogram.dispatcher.filters import Text, IDFilter
from aiogram import Dispatcher
from aiogram.types import Message, ReplyKeyboardRemove
from sqlalchemy.ext.asyncio import AsyncSession
from tgbot.infrastucture.database.models.user import User
from tgbot.infrastucture.database.functions.create_review import create_review
from sqlalchemy.ext.asyncio import AsyncSession
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

from tgbot.keyboards.default.main_menu import main_menu


class review_state(StatesGroup):
    waiting_for_state = State()


async def review_start(message: types.Message):
    await message.answer("Введите отзыв:")
    await review_state.waiting_for_state.set()


async def review_add(message: types.Message, session: AsyncSession, state: FSMContext):
    user = await session.get(User, message.from_user.id)
    await create_review(
        session,
        id_user=message.from_user.id,
        text_review=message.text,
        full_name=user.full_name
    )
    await session.commit()
    await message.answer("Спасибо за обращение",  reply_markup=main_menu)
    await state.finish()


def register_review(dp: Dispatcher):
    dp.register_message_handler(review_start, Text(equals="✍ Написать Отзыв", ignore_case=True), state="*")
    dp.register_message_handler(review_add, state=review_state.waiting_for_state)
