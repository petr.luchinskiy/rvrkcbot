from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text, IDFilter
from aiogram import Dispatcher
from aiogram.types import Message, ReplyKeyboardRemove
from aiogram.utils.markdown import hcode
from sqlalchemy.ext.asyncio import AsyncSession

from tgbot.keyboards.default.main_menu import main_menu


async def contact_view(message: types.Message):
    await message.answer('Вы можете обратится в водоканал по следующим номерам:\n')
    await message.answer(hcode(f'Главный инженер Кривошеев Сергей Александрович тел. 2-47-37\n'
                         f'Заместитель директора по экономическим вопросам и финансам Воропаева Ирина Юрьевна тел.2-49-47\n'
                         f'Главный бухгалтер Елеусенова Наргуль Серикжановна  тел. 2-49-52\n'
                         f'Главный энергетик Кильдишев Сергей Александрович тел. 2-49-46\n'
                         f'Главный механик  Клименко Сергей Александрович  тел. 2-47-97 \n'), reply_markup=main_menu)


def register_contact(dp: Dispatcher):
    dp.register_message_handler(contact_view, Text(equals="📞 Контактная информация", ignore_case=True), state="*")
