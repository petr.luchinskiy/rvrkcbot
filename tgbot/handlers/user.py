from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import Text, IDFilter
from aiogram import Dispatcher
from aiogram.types import Message, ReplyKeyboardRemove
from sqlalchemy.ext.asyncio import AsyncSession

from tgbot.keyboards.default.start_menu import start_menu
from tgbot.keyboards.default.contract_menu import contract_menu
from tgbot.keyboards.default.counter_menu import counter_menu
from tgbot.keyboards.default.main_menu import main_menu
from tgbot.infrastucture.database.functions.users import create_user
from tgbot.infrastucture.database.models.user import User


async def user_start(message: Message, session: AsyncSession):
    user = await session.get(User, message.from_user.id)

    if not user:
        await create_user(
            session,
            telegram_id=message.from_user.id,
            full_name=message.from_user.full_name,
            username=message.from_user.username,
            language_code=message.from_user.language_code,
        )
        await session.commit()
    user = await session.get(User, message.from_user.id)
    user_info = (f"{user.full_name} (@{user.username}).\n"
                 f"Language: {user.language_code}.\n"
                 f"Created at: {user.created_at}.")

    await message.reply("Добро пожаловать, user. \n"
                        "Your info is here: \n\n"
                        f"{user_info}")

    await  message.answer("Выберите пункт меню", reply_markup=main_menu)


async def get_counters(message: types.Message):
    await message.answer("В этом разделе вы можете узнать информацию о счетчиках", reply_markup=counter_menu)


async def get_credit(message: types.Message):
    await message.answer("Здесь работа с начислением на ваших лицевых счетах", reply_markup=ReplyKeyboardRemove())


def register_user(dp: Dispatcher):
    dp.register_message_handler(user_start, commands=["start"], state="*")

