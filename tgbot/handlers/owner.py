from aiogram import types
from aiogram import Dispatcher
from aiogram.types import Message
from aiogram.dispatcher.filters import Text, IDFilter
from aiogram import Dispatcher
from aiogram.types import Message, ReplyKeyboardRemove
from aiogram.utils.markdown import hcode
from sqlalchemy.ext.asyncio import AsyncSession
from aiogram import Dispatcher
from aiogram.types import Message
from sqlalchemy.ext.asyncio import AsyncSession
from tgbot.infrastucture.database.functions.contract import select_contract
from tgbot.infrastucture.database.models.contract import Contract
from tgbot.infrastucture.database.models.owner import Owner
from tgbot.infrastucture.database.functions.owner import *
from sqlalchemy.ext.asyncio import AsyncSession
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

from tgbot.keyboards.default.contract_menu import contract_menu
from tgbot.keyboards.default.main_menu import main_menu


class owner_state(StatesGroup):
    waiting_for_state = State()
    waiting_for_del = State()

async def owner_delete(message: types.Message, session: AsyncSession):
    await message.answer('введите удаляемый лиевой')
    await owner_state.waiting_for_del.set()

async def owner_delete_contract(message: types.Message, session: AsyncSession, state: FSMContext):
    if message.text.isdecimal() == True:
        await delete_owner(session, int(message.from_user.id), int(message.text))
        await message.answer('лицевой удален',reply_markup=contract_menu)
    else:
        await message.answer('не текст')

async def owner_add_contract(message: types.Message, session: AsyncSession):
    await message.answer("Введите номер лицевого который хотите добавить:")
    await owner_state.waiting_for_state.set()

async def owner_back(message: types.Message, session: AsyncSession):
    await message.answer('Выберите пункт в меню ниже', reply_markup=main_menu)

async def owner_start(message: types.Message, session: AsyncSession):
    count_contract = await select_default_owner(session, int(message.from_user.id))
    if len(count_contract) < 1:
        await message.answer("Лицевой не привязан, пожалуйста введите номер вашего лицевого счета")
        await message.answer("Введите ваш лицевой:")
        await owner_state.waiting_for_state.set()
    else:
        for i in range(len(count_contract)):
            await message.answer(hcode(f'лицевой номер {count_contract[i].id_contract} и адрес {count_contract[i].address}\n'), reply_markup=contract_menu)
            await owner_state.waiting_for_state.set()



async def owner_add(message: types.Message, session: AsyncSession, state: FSMContext):
    # если передали число
    if message.text.isdecimal() == True:
        issuecontract = await select_contract(session, int(message.text))

        if issuecontract is not None:
            if issuecontract.flat is not None:
                addr = 'ул.' + issuecontract.street.strip() + ' кв.' + issuecontract.house.strip() + ' д.' + issuecontract.flat.strip()
            else:
                addr = 'ул.' + issuecontract.street.strip() + ' кв.' + issuecontract.house.strip()
            await create_owner(
                session,
                id_user=message.from_user.id,
                id_contract=int(message.text),
                address=addr
            )
            await session.commit()
            await message.answer("Лицевой счет привязан", reply_markup=main_menu)
            await state.finish()
        else:
            await message.answer('Лицевой не найден, если вы считаете что такой лицевой есть '
                                 '\n обратитесь в ркц тел 4-17-02', reply_markup=main_menu)
        await state.finish()
    else:
        await message.answer('вы ввели что угодно но не число', reply_markup=main_menu)
        await state.finish()




def register_owner(dp: Dispatcher):
    dp.register_message_handler(owner_start, Text(equals="📋 Лицевой счет", ignore_case=True), state="*")
    dp.register_message_handler(owner_delete, Text(equals="❌ Удалить лицевой счет", ignore_case=True), state="*")
    dp.register_message_handler(owner_add_contract, Text(equals="➕ Добавить лицевой счет", ignore_case=True), state="*")
    dp.register_message_handler(owner_back, Text(equals="Назад ⬅", ignore_case=True), state="*")
    dp.register_message_handler(owner_add, state=owner_state.waiting_for_state)
    dp.register_message_handler(owner_delete_contract, state=owner_state.waiting_for_del)

